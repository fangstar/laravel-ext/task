CREATE TABLE `timed_task_def` (
    `timed_task_def_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
    `timed_task_key` varchar(63) DEFAULT NULL COMMENT '任务KEY',
    `timed_task_name` varchar(63) NOT NULL COMMENT '任务名称',
    `timed_task_desc` varchar(255) DEFAULT NULL COMMENT '任务描述',
    `timed_task_class` varchar(127) NOT NULL COMMENT '任务执行class',
    `timed_task_method` varchar(63) NOT NULL DEFAULT 'exec' COMMENT '任务执行method',
    `timed_task_command` varchar(127) DEFAULT NULL COMMENT '执行命令',
    `timed_task_frequency` varchar(63) NOT NULL COMMENT '任务执行频率',
    `timed_task_params` varchar(127) DEFAULT NULL COMMENT '任务时间参数',
    `timed_task_exec_params` text COMMENT '任务执行参数',
    `timed_task_start_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次任务执行开始时间',
    `timed_task_end_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次任务执行结束时间',
    `timed_task_cast` int(11) NOT NULL DEFAULT '0' COMMENT '上次任务花费时间 秒',
    `timed_task_exec_proj` varchar(63) NOT NULL DEFAULT 'erp' COMMENT '任务所在项目erp  base sys',
    `timed_task_exec_module` varchar(63) NOT NULL DEFAULT 'other' COMMENT '任务所在模块',
    `timed_task_exec_order` int(11) NOT NULL DEFAULT '1' COMMENT '同一执行频次的任务执行顺序 由小到大执行',
    `timed_task_status` tinyint(11) NOT NULL DEFAULT '1' COMMENT '任务状态 1：启用  2：停用  3：作废',
    `timed_task_exec_status` tinyint(11) NOT NULL DEFAULT '3' COMMENT '任务执行状态  1：执行中   2：执行出错  3：完成',
    `timed_task_exec_timeout` int(11) NOT NULL DEFAULT '0' COMMENT '执行超时时间，重复执行的时候超过该时间还未完成也会重新执行，0为不超时一直等待',
    `timed_task_exec_err_msg` text COMMENT '执行出错时的错误消息',
    `timed_task_allow_manual_exec` tinyint(4) NOT NULL DEFAULT '1' COMMENT '允许手工执行  1：是 2：否',
    `delete_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记(0.有效，1.回收站，2.删除)',
    `created_at` int(10) unsigned NOT NULL COMMENT '创建记录时间',
    `updated_at` int(10) unsigned DEFAULT NULL COMMENT '更新时间',
    `deleted_at` int(10) DEFAULT NULL COMMENT ' 删除记录时间 ',
    PRIMARY KEY (`timed_task_def_id`),
    UNIQUE KEY `idx_timed_task_key_unique` (`timed_task_key`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='定时任务定义表';


CREATE TABLE `timed_task_record` (
    `timed_task_record_id` char(32) NOT NULL COMMENT '定时任务执行记录ID',
    `timed_task_def_id` int(11) NOT NULL COMMENT '任务定义ID',
    `task_exec_emp_id` int(11) NOT NULL DEFAULT '-99' COMMENT '任务执行人ID',
    `task_exec_emp_name` varchar(31) NOT NULL DEFAULT '系统' COMMENT '执行人名称',
    `task_start_at` bigint(20) NOT NULL COMMENT '任务执行开始时间',
    `task_end_at` bigint(20) DEFAULT NULL COMMENT '任务执行结束时间',
    `task_cast` int(11) NOT NULL DEFAULT '0' COMMENT '任务执行时长  秒',
    `task_exec_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '任务执行状态   1：执行中   2：执行出错  3：完成',
    `task_total_cnt` int(11) NOT NULL DEFAULT '1' COMMENT '总共需要执行的任务数',
    `task_exec_cnt` int(11) NOT NULL DEFAULT '0' COMMENT '执行的任务数',
    `task_success_cnt` int(11) NOT NULL DEFAULT '0' COMMENT '成功执行的任务数',
    `task_err_msg` text COMMENT '任务执行错误的错误消息',
    `delete_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记(0.有效，1.回收站，2.删除)',
    `created_at` int(10) unsigned NOT NULL COMMENT '创建记录时间',
    `updated_at` int(10) unsigned DEFAULT NULL COMMENT '更新时间',
    `deleted_at` int(10) DEFAULT NULL COMMENT ' 删除记录时间 ',
    PRIMARY KEY (`timed_task_record_id`),
    KEY `idx_timed_task_def_id_created_at` (`timed_task_def_id`,`created_at`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `timed_task_log` (
    `timed_task_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务执行日志ID',
    `timed_task_def_id` int(11) NOT NULL COMMENT '任务定义ID',
    `timed_task_record_id` char(32) NOT NULL COMMENT '定时任务执行记录ID',
    `log_type` varchar(15) NOT NULL DEFAULT 'info' COMMENT '任务记录类型 info error warning',
    `log_order` int(1) NOT NULL DEFAULT '1' COMMENT '日志排序 由小到大',
    `log_content` text COMMENT '日志内容',
    `created_at` int(10) unsigned NOT NULL COMMENT '创建记录时间',
    `updated_at` int(10) unsigned DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`timed_task_log_id`),
    KEY `idx_timed_task_record_id` (`timed_task_record_id`,`log_order`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;