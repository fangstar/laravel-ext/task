<?php

namespace Fstar\Task\Model;


class TimedTaskRecordM extends BaseModel {
    protected $table = 'timed_task_record';
    protected $primaryKey = 'timed_task_record_id';
    protected $keyType = 'string';
}