<?php

namespace Fstar\Task\Api;

interface TaskRecordInterface {
    public function __construct(array $config);

    public function recordFindByDefId(int $def_id, int $skip, int $limit): array;

    public function recordUpdate(string $rec_id, array $record);

    public function recordSave(array $record);

}