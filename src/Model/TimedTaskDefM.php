<?php

namespace Fstar\Task\Model;


class TimedTaskDefM extends BaseModel {
    protected $table = 'timed_task_def';
    protected $primaryKey = 'timed_task_def_id';

    const TASK_STATUS_ENABLE = 1;
    const TASK_STATUS_DISABLE = 2;
    const TASK_STATUS_INVALID = 3;

    const TASK_EXEC_STATUS_RUNNING = 1;
    const TASK_EXEC_STATUS_ERROR = 2;
    const TASK_EXEC_STATUS_FINISH = 3;

    const ALLOW_MANUAL_EXEC_YES = 1;

    public function findByFrequencyAndParams($frequency, $task_params, $task_proj) {
        $builder = self::where('timed_task_exec_proj', $task_proj)
                       ->where('timed_task_status', self::TASK_STATUS_ENABLE)
                       ->where('delete_flag', 0)
                       ->where('timed_task_frequency', $frequency);
        if(!empty($task_params)) {
            $builder->where('timed_task_params', $task_params);
        }
        return $builder->orderBy('timed_task_exec_order', 'asc')->get()->toArray();
    }
}