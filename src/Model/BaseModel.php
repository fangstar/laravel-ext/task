<?php

namespace Fstar\Task\Model;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {
    protected $dateFormat = 'U';
    protected $casts = [
        'created_at' => 'datetime:U',
        'updated_at' => 'datetime:U',
        'deleted_at' => 'datetime:U',
    ];
}