<?php

namespace Fstar\Task\Impl;

use Fstar\Task\Api\TaskRecordInterface;
use Fstar\Task\Model\TimedTaskRecordM;

class TaskRecordDBImpl implements TaskRecordInterface {
    private $field_map = [];
    private $db_conn;

    public function __construct(array $config) {
        $this->db_conn   = data_get($config, 'db_conn');
        $this->field_map = [
            'id'               => 'timed_task_record_id',
            'exec_emp_id'      => 'task_exec_emp_id',
            'exec_emp_name'    => 'task_exec_emp_name',
            'start_at'         => 'task_start_at',
            'end_at'           => 'task_end_at',
            'exec_cast'        => 'task_cast',
            'task_id'          => 'timed_task_def_id',
            'exec_status'      => 'task_exec_status',
            'error_msg'        => 'task_err_msg',
            'exec_success_cnt' => 'task_success_cnt',
            'exec_task_cnt'    => 'task_exec_cnt',
            'find_task_cnt'    => 'task_total_cnt',
            'delete_flag'      => 'delete_flag',
            'created_at'       => 'created_at'
        ];
    }

    public function recordFindByDefId(int $def_id, int $skip, int $limit): array {
        $builder = TimedTaskRecordM::on($this->db_conn)
                                   ->where('timed_task_def_id', $def_id);
        $total   = $builder->count('timed_task_def_id');
        $data    = $builder->orderBy('created_at', 'desc')
                           ->skip($skip)
                           ->take($limit)
                           ->get()->toArray();
        return ['data' => $data, 'total' => $total];
    }

    public function recordUpdate(string $rec_id, array $record) {
//        $params = [];
//        $record['timed_task_record_id'] = $rec_id;
//        foreach($this->field_map as $field => $new_field) {
//            $params[$new_field] = data_get($record, $field);
//        }
        return TimedTaskRecordM::on($this->db_conn)
                               ->where('timed_task_record_id', $rec_id)
                               ->update($record);
    }

    public function recordSave(array $record) {
//        $params = [];
//        foreach($this->field_map as $field => $new_field) {
//            $params[$new_field] = data_get($record, $field);
//        }
        TimedTaskRecordM::on($this->db_conn)
                        ->insert($record);
    }
}
