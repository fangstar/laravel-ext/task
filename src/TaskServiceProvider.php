<?php

namespace Fstar\Task;

use Fstar\Task\Task\TaskDriverParseService;
use Fstar\Task\Task\TimedTaskExecCommand;
use Illuminate\Support\ServiceProvider;

class TaskServiceProvider extends ServiceProvider {
    public function boot() {
        $this->publishes([__DIR__."/config/fstar-task.php" => config_path(Constants::conf_name.".php")]);
    }

    public function register() {
        $this->mergeConfigFrom(__DIR__."/config/fstar-task.php", Constants::conf_name);
        $user_config = $this->app['config'][Constants::conf_name];
        $this->app->bind(Constants::lib_task_exec, function($app) use ($user_config) {
            return new TimedTaskExecCommand($user_config);
        });
        $this->app->singleton(Constants::lib_task_driver, function($app) use ($user_config) {
            return new TaskDriverParseService($user_config);
        });
    }

    public function provides() {
        return [
            Constants::lib_task_exec,
            Constants::lib_task_driver
        ];
    }
}