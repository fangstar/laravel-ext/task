<?php

namespace Fstar\Task\Model;


class TimedTaskLogM extends BaseModel {
    protected $table = 'timed_task_log';
    protected $primaryKey = 'timed_task_log_id';
}