<?php

namespace Fstar\Task\Task;

use Fstar\Task\Model\TimedTaskDefM;
use Illuminate\Support\Facades\Redis;

class TimedTaskExecService extends BaseTaskService {

    private $task_proj;
    private $sys_proj;
    private $sys_proj_module;
    private $redis_conn;
    private $db_conn;
    private $now;

    public function __construct($config) {
        $this->now = time();
        $this->db_conn = data_get($config, 'db_conn');
        $this->sys_proj = data_get($config, 'task_def.sys_proj', 'sys');
        $this->sys_proj_module = data_get($config, 'task_def.proj_module', 'sys');
        $this->task_proj = data_get($config, 'task_def.proj', 'erp');
        $this->redis_conn = Redis::connection(data_get($config, 'redis_conn'));
        parent::__construct('timed_task_exec');
    }

    public function exec() {

    }

    /**
     * 手动执行定时任务
     */
    public function manualExec($task_def_id) {
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);

        $task = TimedTaskDefM::on($this->db_conn)->find($task_def_id);
        if($task == null) {
            throw new TaskException("任务还未配置，任务ID：{$task_def_id}");
        }
        if($task->timed_task_allow_manual_exec != TimedTaskDefM::ALLOW_MANUAL_EXEC_YES) {
            throw new TaskException("改任务运行风险过大，不能手动执行，任务ID：{$task_def_id}");
        }
        $tasks = $this->execTask([$task]);
        $this->finish();
        return $tasks;
    }


    /**
     * (new Fstar\Task\Task\TimedTaskExecService())->everyMinute();
     */
    public function everyMinute() {
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);
        $tasks = $this->queryTask('everyMinute');
        $total = count($tasks);
        $this->execTask($tasks, $total);
        $this->finish();
    }

    /**
     * (new Fstar\Task\Task\TimedTaskExecService())->everyFiveMinutes();
     */
    public function everyFiveMinutes() {
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);
        $tasks = $this->queryTask('everyFiveMinutes');
        $total = count($tasks);
        $this->execTask($tasks, $total);
        $this->finish();
    }

    /**
     * (new Fstar\Task\Task\TimedTaskExecService())->everyTenMinutes();
     */
    public function everyTenMinutes() {
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);
        $tasks = $this->queryTask('everyTenMinutes');
        $total = count($tasks);
        $this->execTask($tasks, $total);
        $this->finish();
    }

    /**
     * (new Fstar\Task\Task\TimedTaskExecService())->everyFifteenMinutes();
     */
    public function everyFifteenMinutes() {
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);
        $tasks = $this->queryTask('everyFifteenMinutes');
        $total = count($tasks);
        $this->execTask($tasks, $total);
        $this->finish();
    }

    /**
     * (new Fstar\Task\Task\TimedTaskExecService())->everyThirtyMinutes();
     */
    public function everyThirtyMinutes() {
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);
        $tasks = $this->queryTask('everyThirtyMinutes');
        $total = count($tasks);
        $this->execTask($tasks, $total);
        $this->finish();
    }

    /**
     * (new Fstar\Task\Task\TimedTaskExecService())->hourly();
     */
    public function hourly($now) {
        $hour = date('H', $now);
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);
        $tasks = $this->queryTask('hourly');
        $total = count($tasks);
        $exec_tasks = [];
        foreach($tasks as $task) {
            if(empty($task->timed_task_params) || strpos($task->timed_task_params, $hour) !== false) {
                $exec_tasks[] = $task;
            }
        }
        $this->execTask($exec_tasks, $total);
        $this->finish();
    }

    /**
     * (new Fstar\Task\Task\TimedTaskExecService())->setTaskKey();
     */
    public function hourlyAt($now) {
        $minute = date('i', $now);
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);
        $tasks = $this->queryTask('hourlyAt');
        $total = count($tasks);
        $exec_tasks = [];
        foreach($tasks as $task) {
            if(!empty($task->timed_task_params) && $task->timed_task_params == $minute) {
                $exec_tasks[] = $task;
            }
        }
        if(count($exec_tasks) <= 0) {
            $this->saveRecord('delete');
        }
        $this->execTask($exec_tasks, $total);
        $this->finish();
    }

    /**
     * (new Fstar\Task\Task\TimedTaskExecService())->daily();
     */
    public function daily() {
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);
        $tasks = $this->queryTask('daily');
        $total = count($tasks);
        $this->execTask($tasks, $total);
        $this->finish();
    }

    /**
     * (new Fstar\Task\Task\TimedTaskExecService())->dailyAt();
     */
    public function dailyAt($now) {
        $date = date('H:i:00', $now);
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);
        $tasks = $this->queryTask('dailyAt');
        $total = count($tasks);
        $exec_tasks = [];
        foreach($tasks as $task) {
            if(!empty($task->timed_task_params) && strpos($task->timed_task_params, $date) !== false) {
                $exec_tasks[] = $task;
            }
        }
        if(count($exec_tasks) <= 0) {
            $this->saveRecord('delete');
        }
        $this->execTask($exec_tasks, $total);
        $this->finish();
    }

    /**
     * (new Fstar\Task\Task\TimedTaskExecService())->withoutOverlapping();
     */
    public function withoutOverlapping() {
        $now = time();
        $this->initTaskMonitor(__METHOD__, true, $this->sys_proj, $this->sys_proj_module);
        $tasks = $this->queryTask('withoutOverlapping');
        $total = count($tasks);
        $exec_tasks = [];
        foreach($tasks as $task) {
            if($task->timed_task_exec_status == TimedTaskDefM::TASK_EXEC_STATUS_RUNNING && $now < $task->timed_task_exec_timeout + $task->timed_task_start_at) {
                $this->info(json_encode($task));
                continue;
            }
            $exec_tasks[] = $task;
        }
        $this->execTask($exec_tasks, $total);
        $this->finish();
    }

    private function execTask($tasks, $find_task_cnt = null) {
        ini_set('max_execution_time', '0');
        $index = 1;
        $total = count($tasks);
        $find_task_cnt = empty($find_task_cnt) ? $total : $find_task_cnt;
        $this->info("Wait to exec task cnt:{$total}");
        $ret_tasks = [];
        $success_cnt = 0;
        foreach($tasks as $task) {
            $this->info("Exec task {$index}/{$total} Task ID:{$task->timed_task_def_id}");
            $method = $task->timed_task_method;
            $class = $task->timed_task_class;
            $task_key = $task->timed_task_key;
            $task_exec_params = $task->timed_task_exec_params;
            $redis_Key = "Task_repeat_exec:{$task_key}";
            if($this->hasRunning($redis_Key, $task->timed_task_exec_timeout)) {
                $this->info("Task {$class}('{$method}') has running, do skip.");
                continue;
            }
            $this->info("New task {$index}/{$total} new {$class}('{$method}')");
            $index ++;
            try {
                $oReflectionClass = new \ReflectionClass($class);
                $svc = $oReflectionClass->newInstance();
                if(!($svc instanceof BaseTaskService)) {
                    $this->warn("Task {$class} isn't instance of BaseTaskService");
                    continue;
                }
                if(!$svc->hasTaskModel()) {
                    $svc->setTaskKey($task_key);
                }
                if($svc->hasTaskModel()) {
                    $ret_tasks[] = $svc->getTaskModel();
                }
                if(empty($task_exec_params)) {
                    $this->info("Exec {$class}->{$method}()");
                    $svc->$method();
                } else {
                    $this->info("Exec {$class}->{$method}($task_exec_params)");
                    $svc->$method($task_exec_params);
                }
                $success_cnt ++;
            } catch(\Exception $ex) {
                $this->setError($ex->getMessage());
            }
            $this->redis_conn->del($redis_Key);
        }
        $this->saveRecord('update', ['task_total_cnt' => $find_task_cnt, 'task_exec_cnt' => $total, 'task_success_cnt' => $success_cnt]);
        return $ret_tasks;
    }

    private function queryTask($timed_task_frequency, $task_params = null) {
        $builder = TimedTaskDefM::on($this->db_conn)->where('timed_task_exec_proj', $this->task_proj)
                                ->where('timed_task_status', TimedTaskDefM::TASK_STATUS_ENABLE)
                                ->where('delete_flag', 0)
                                ->where('timed_task_frequency', $timed_task_frequency);
        if(!empty($task_params)) {
            $builder->where('timed_task_params', $task_params);
        }
        $tasks = $builder->orderBy('timed_task_exec_order', 'asc')->get();

        $this->info("Find task cnt:".count($tasks));
        return $tasks;
    }

    private function hasRunning($redis_key, $time_out) {
        $this->redis_conn->rpush($redis_key, $this->now);
        $list = $this->redis_conn->lrange($redis_key, 0, - 1);
        $running_cnt = count($list) - 1;
        $this->info("Find running task cnt:{$running_cnt}");
        //该任务上次执行未完成，还没到执行超时时间
        if($running_cnt >= 1 && $list[0] > $this->now - $time_out) {
            return true;
        }
        return false;
    }
}
