<?php

namespace Fstar\Task\Task;

use Fstar\Task\Constants;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Task\Task\TimedTaskExecService
 *
 * @method static run(Schedule $schedule)
 * @method static manualExec(int $task_def_id)
 */
class FsTaskAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_task_exec;
    }
}