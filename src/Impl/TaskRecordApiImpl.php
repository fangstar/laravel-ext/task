<?php

namespace Fstar\Task\Impl;

use Fstar\Client\Mongo\MongoAlias;
use Fstar\Task\Api\TaskRecordInterface;

class TaskRecordApiImpl implements TaskRecordInterface {
    private $field_map = [];

    public function __construct(array $config) {
        $this->field_map = [
            'id'              => 'timed_task_record_id',
            'exec_emp_id'      => 'task_exec_emp_id',
            'exec_emp_name'    => 'task_exec_emp_name',
            'start_at'         => 'task_start_at',
            'end_at'           => 'task_end_at',
            'exec_cast'        => 'task_cast',
            'task_id'          => 'timed_task_def_id',
            'exec_status'      => 'task_exec_status',
            'error_msg'        => 'task_err_msg',
            'exec_success_cnt' => 'task_success_cnt',
            'exec_task_cnt'    => 'task_exec_cnt',
            'find_task_cnt'    => 'task_total_cnt',
            'delete_flag'      => 'log_order',
            'created_at'       => 'delete_flag'
        ];
    }

    public function recordFindByDefId(int $def_id, int $skip, int $limit): array {
        $page = ($skip / $limit) + 1;
        $rec_list = MongoAlias::taskLogFindRecord(['timed_task_def_id' => $def_id, 'page' => $page, 'pagesize' => $limit]);
        $ret = [];
        foreach($rec_list as $idx => $rec) {
            $ret[$idx] = [];
            foreach($this->field_map as $field => $new_field) {
                $ret[$idx][$new_field] = data_get($rec, $field);
            }
        }
        return $ret;
    }

    public function recordUpdate(string $rec_id, array $record) {
        $params = [];
        $record['timed_task_record_id'] = $rec_id;
        foreach($this->field_map as $field => $new_field) {
            $params[$field] = data_get($record, $new_field);
        }
        return MongoAlias::taskLogUpdRecord($params);
    }

    public function recordSave(array $record) {
        $params = [];
        foreach($this->field_map as $field => $new_field) {
            $params[$field] = data_get($record, $new_field);
        }
        MongoAlias::taskLogAddRecord($params);
    }
}