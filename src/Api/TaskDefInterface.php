<?php

namespace Fstar\Task\Api;

interface TaskDefInterface {
    public function __construct(array $config);

    public function queryList(array $params, array $page, array $sort);

    public function update(array $task_info);
}
