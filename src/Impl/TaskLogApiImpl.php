<?php

namespace Fstar\Task\Impl;

use Fstar\Client\Mongo\MongoAlias;
use Fstar\Task\Api\TaskLogInterface;

class TaskLogApiImpl implements TaskLogInterface {

    private $field_map = [];

    public function __construct(array $config) {
        $this->field_map = [
            'task_id'        => 'timed_task_log_id',
            'task_record_id' => 'timed_task_record_id',
            'trace_type'     => 'log_type',
            'trace_msg'      => 'log_content',
            'trace_order'    => 'log_order',
            'created_at'     => 'created_at'
        ];
    }

    public function logFind(string $rec_id): array {
        $log_arry = MongoAlias::taskLogFind($rec_id);
        $ret = [];
        foreach($log_arry as $idx => $log) {
            $ret[$idx] = [];
            foreach($this->field_map as $field => $new_field) {
                $ret[$idx][$new_field] = data_get($log, $field);
            }
        }
        return $ret;
    }

    public function logSave(array $log) {
        $params = [];
        foreach($this->field_map as $field => $new_field) {
            $params[$field] = data_get($log, $new_field);
        }
        return MongoAlias::taskLogAddLog($params);
    }
}