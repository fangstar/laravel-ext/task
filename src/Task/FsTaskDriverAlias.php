<?php

namespace Fstar\Task\Task;

use Fstar\Task\Constants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Task\Task\TaskDriverParseService
 *
 * @method static \Fstar\Task\Api\TaskDefInterface getTaskDefDriver()
 * @method static \Fstar\Task\Api\TaskRecordInterface getTaskRecordDriver()
 * @method static \Fstar\Task\Api\TaskLogInterface getTaskLogDriver()
 */
class FsTaskDriverAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_task_driver;
    }
}