<?php

namespace Fstar\Task\Task;


use Illuminate\Console\Scheduling\Schedule;

class TimedTaskExecCommand {

    private $config;

    public function __construct($config) {
        $this->config = $config;
    }

    public function run(Schedule $schedule) {
        $now = time();
        $config = $this->config;
        $schedule->call(function() use ($config) {
            (new TimedTaskExecService($config))->everyMinute();
        })->everyMinute();
        $schedule->call(function() use ($now, $config) {
            (new TimedTaskExecService($config))->hourlyAt($now);
        })->everyMinute();
        $schedule->call(function() use ($now, $config) {
            (new TimedTaskExecService($config))->dailyAt($now);
        })->everyMinute();
        $schedule->call(function() use ($config) {
            (new TimedTaskExecService($config))->withoutOverlapping();
        })->everyMinute();
        $schedule->call(function() use ($config) {
            (new TimedTaskExecService($config))->everyFiveMinutes();
        })->everyFiveMinutes();
        $schedule->call(function() use ($config) {
            (new TimedTaskExecService($config))->everyTenMinutes();
        })->everyTenMinutes();
        $schedule->call(function() use ($config) {
            (new TimedTaskExecService($config))->everyFifteenMinutes();
        })->everyFifteenMinutes();
        $schedule->call(function() use ($config) {
            (new TimedTaskExecService($config))->everyThirtyMinutes();
        })->everyThirtyMinutes();
        $schedule->call(function() use ($now, $config) {
            (new TimedTaskExecService($config))->hourly($now);
        })->hourly();
        $schedule->call(function() use ($config) {
            (new TimedTaskExecService($config))->daily();
        })->daily();
    }

    public function manualExec($task_def_id) {
        return (new TimedTaskExecService($this->config))->manualExec($task_def_id);
    }

}
