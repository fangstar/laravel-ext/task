<?php

namespace Fstar\Task\Impl;

use Fstar\Task\Api\TaskDefInterface;
use Fstar\Task\Model\TimedTaskDefM;

class TaskDefDBImpl implements TaskDefInterface {

    private $db_conn;

    public function __construct(array $config) {
        $this->db_conn = data_get($config, 'db_conn');
    }

    public function queryList(array $params, array $page, array $sort) {
        $query_conf = [
            'timed_task_def_id'      => [],
            'timed_task_name'        => ['query' => 'like'],
            'timed_task_frequency'   => [],
            'timed_task_exec_proj'   => [],
            'timed_task_exec_module' => [],
            'timed_task_status'      => [],
            'timed_task_exec_status' => [],
        ];
        $builder    = TimedTaskDefM::where('delete_flag', 0);
        foreach ($params as $key => $val) {
            if ($val !== null && $val !== '' && array_key_exists($key, $query_conf)) {
                $query = data_get($query_conf, "{$key}.query", 'where');
                $field = data_get($query_conf, "{$key}.field", $key);
                if ($query == 'like') {
                    $builder->where($field, 'like', "%{$val}%");
                } else {
                    $builder->where($field, $val);
                }
            }
        }
        $total = $builder->count('timed_task_def_id');
        $data  = $builder->orderBy(data_get($sort, 'field', 'timed_task_def_id'), data_get($sort, 'direction', 'asc'))
                         ->skip(data_get($page, 'skip', 0))
                         ->take(data_get($page, 'pagesize', 200))
                         ->get();
        return ['data' => $data, 'total' => $total];
    }

    public function update(array $task_info) {
        TimedTaskDefM::on($this->db_conn)
                     ->where('timed_task_def_id', $task_info['timed_task_def_id'])
                     ->update($task_info);
    }
}
