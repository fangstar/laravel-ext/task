<?php

namespace Fstar\Task\Task;

use Fstar\Task\Model\TimedTaskDefM;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

abstract class BaseTaskService {

    public $monolog;
    private $task_model = null;
    private $record_id;
    private $start_at = 0;
    private $end_at = 0;
    private $order = 1;
    private $db_conn = null;
    private $task_def_driver;
    private $task_record_driver;
    private $task_log_driver;

    public function __construct($log_name) {
        $this->task_def_driver = FsTaskDriverAlias::getTaskDefDriver();
        $this->task_record_driver = FsTaskDriverAlias::getTaskRecordDriver();
        $this->task_log_driver = FsTaskDriverAlias::getTaskLogDriver();
        $this->order = 1;
        $this->record_id = $this->getUuid();
        $this->db_conn = config('fstar-task.db_conn');
        //日志
        $this->initLog($log_name, get_class($this), 'exec');
    }

    private function getUuid() {
        return md5(uniqid(mt_rand(), true));
    }

    public abstract function exec();

    private function initLog($log_name, $class_path, $method) {
        $date = date('Ymd');
        $dir_sep = DIRECTORY_SEPARATOR;
        $path_arry = $this->classToPath($class_path);
        $log_name = empty($log_name) ? $path_arry['name'] : $log_name;
        $this->monolog = new Logger($log_name);
        $streamHandler = new StreamHandler(storage_path("logs{$dir_sep}{$path_arry['path']}_{$method}_{$date}.log"), Logger::INFO);
        $this->monolog->pushHandler($streamHandler);
    }

    private function classToPath($class_path) {
        $dir_sep = DIRECTORY_SEPARATOR;
        $clazz_arry = explode('\\', $class_path);
        $len = count($clazz_arry);
        $pos = $len - 3;
        $paths = [];
        for($idx = $pos; $idx < $len; $idx ++) {
            $paths[] = strtolower(preg_replace('/(?<=[a-z])([A-Z])/', '_$1', $clazz_arry[$idx]));;
        }
        $path = implode($dir_sep, $paths);
        if(strpos($path, 'task') != 0) {
            $path = "task{$dir_sep}{$path}";
        }
        return ['path' => $path, 'name' => $paths[count($paths) - 1]];
    }

    protected function initTaskMonitor($method, $reset_log = true, $proj = null, $module = null) {
        if(!$this->hasTaskModel()) {
            $method_arry = explode('::', $method);
            if(count($method_arry) != 2) {
                return;
            }
            $clazz = $method_arry[0];
            $method = $method_arry[1];
            $builder = TimedTaskDefM::on($this->db_conn)->where('timed_task_class', $clazz)
                                    ->where('timed_task_method', $method)
                                    ->where('timed_task_class', $clazz)
                                    ->where('delete_flag', 0);
            if(!empty($proj)) {
                $builder->where('timed_task_exec_proj', $proj);
            }

            if(!empty($module)) {
                $builder->where('timed_task_exec_module', $module);
            }
            $task_arry = $builder->get(['timed_task_key'])->toArray();

            if(count($task_arry) != 1) {
                return;
            }
            if($reset_log) {
                $this->initLog($method_arry[1], $method_arry[0], $method_arry[1]);
            }
            $this->setTaskKey($task_arry[0]['timed_task_key']);
        }
    }

    protected function setTaskKey($task_key = null, $default = null) {
        $this->start_at = time();
        $this->monolog->info('Begin');
        $task_key = empty($task_key) ? $default : $task_key;
        $this->task_model = TimedTaskDefM::on($this->db_conn)->where('timed_task_key', $task_key)->first();
        if(!empty($this->task_model)) {
            $this->updTaskStatus(['timed_task_def_id'       => $this->task_model->timed_task_def_id,
                                  'timed_task_start_at'     => $this->start_at,
                                  'timed_task_end_at'       => 0,
                                  'timed_task_cast'         => 0,
                                  'timed_task_exec_err_msg' => null,
                                  'timed_task_exec_status'  => TimedTaskDefM::TASK_EXEC_STATUS_RUNNING]);
            $this->saveRecord('insert', [
                'timed_task_record_id' => $this->record_id,
                'timed_task_def_id'    => $this->task_model->timed_task_def_id,
                'task_exec_emp_id'     => session('emp_id', - 99),
                'task_exec_emp_name'   => session('emp_name', '系统'),
                'task_start_at'        => $this->start_at,
                'task_end_at'          => 0,
                'task_cast'            => 0,
                'task_exec_status'     => TimedTaskDefM::TASK_EXEC_STATUS_RUNNING,
                'task_err_msg'         => null,
                'delete_flag'          => 0,
                'created_at'           => $this->start_at,
                'updated_at'           => $this->start_at
            ]);
            $this->saveTrace('start', 'Task Start');
        }
    }

    public function hasTaskModel() {
        return !empty($this->task_model);
    }

    public function getTaskModel() {
        return $this->task_model;
    }

    public function debug($msg, array $context = []) {
        $this->saveTrace('debug', $msg, $context);
    }

    public function info($msg, array $context = []) {
        $this->saveTrace('info', $msg, $context);
    }

    public function warn($msg, array $context = []) {
        $this->saveTrace('warning', $msg, $context);
    }

    public function warning($msg, array $context = []) {
        $this->saveTrace('warning', $msg, $context);
    }

    public function error($msg, array $context = []) {
        $this->saveTrace('error', $msg, $context);
    }

    public function setError($msg, array $context = []) {
        if(!empty($this->task_model)) {
            $this->updTaskStatus(['timed_task_def_id'       => $this->task_model->timed_task_def_id,
                                  'timed_task_exec_status'  => TimedTaskDefM::TASK_EXEC_STATUS_ERROR,
                                  'timed_task_exec_err_msg' => $msg
                                 ]);
        }
        $this->saveRecord('update', ['task_exec_status' => TimedTaskDefM::TASK_EXEC_STATUS_ERROR, 'task_err_msg' => $msg]);
        $this->saveTrace('error', $msg, $context);
    }

    protected function finish($msg = null) {
        $this->end_at = time();
        if(!empty($this->task_model)) {
            $this->updTaskStatus(['timed_task_def_id'      => $this->task_model->timed_task_def_id,
                                  'timed_task_end_at'      => $this->end_at,
                                  'timed_task_cast'        => $this->end_at - $this->start_at,
                                  'timed_task_exec_status' => TimedTaskDefM::TASK_EXEC_STATUS_FINISH]);
            $this->saveRecord('update', [
                'task_end_at'      => $this->end_at,
                'task_cast'        => $this->end_at - $this->start_at,
                'task_exec_status' => TimedTaskDefM::TASK_EXEC_STATUS_FINISH]);
            $this->saveTrace('end', (empty($msg) ? 'Task End' : $msg));
        }
        $this->monolog->info('End');
    }

    private function updTaskStatus($task) {
        if(empty($this->task_def_driver)) {
            return;
        }
        try {
            $this->task_def_driver->update($task);
        } catch(\Exception $ex) {
            $this->monolog->error($ex->getMessage());
        }
    }

    protected function saveRecord($type, $record = []) {
        if(empty($this->task_record_driver)) {
            return;
        }
        try {
            if($type == 'insert') {
                $this->task_record_driver->recordSave($record);
            } else {
                if($type == 'update') {
                    $this->task_record_driver->recordUpdate($this->record_id, $record);
                } else {
                    $this->task_record_driver->recordUpdate($this->record_id, ['delete_flag' => 1]);
                }
            }
        } catch(\Exception $ex) {
            $this->monolog->error($ex->getMessage());
        }
    }

    private function saveTrace($type, $msg, array $context = []) {
        $this->end_at = time();
        if(in_array($type, ['debug', 'info', 'warning', 'error'])) {
            $this->monolog->$type($msg, $context);
        }
        if(!empty($this->task_model)) {
            $data = [
                'timed_task_def_id'    => $this->task_model->timed_task_def_id,
                'timed_task_record_id' => $this->record_id,
                'log_type'             => $type,
                'log_content'          => $msg,
                'log_order'            => $this->order ++,
                'created_at'           => time()
            ];
            try {
                $this->task_log_driver->logSave($data);
            } catch(\Exception $ex) {
                $this->monolog->error($ex->getMessage());
            }
        }
    }

}
