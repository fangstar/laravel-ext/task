<?php
return [
    'db_conn'     => env('TASK_DB_CONN', 'mysql'),
    'redis_conn'  => env('TASK_REDIS_CONN', 'default'),
    'task_def'    => [
        'proj'        => env('TASK_PROJ', 'erp'),
        'sys_proj'    => env('TASK_SYS_PROJ', 'sys'),
        'proj_module' => env('TASK_SYS_PROJ_MODULE', 'erp_task'),
        'driver'      => 'db',
        'drivers'     => [
            'db' => \Fstar\Task\Impl\TaskDefDBImpl::class
        ]
    ],
    'task_record' => [
        'driver'  => 'db',
        'drivers' => [
            'db'  => \Fstar\Task\Impl\TaskRecordDBImpl::class,
            'api' => \Fstar\Task\Impl\TaskRecordApiImpl::class,
        ]
    ],
    'task_log'    => [
        'driver'  => 'disk',
        'drivers' => [
            'disk' => \Fstar\Task\Impl\TaskLogDiskImpl::class,
            'api'  => \Fstar\Task\Impl\TaskLogApiImpl::class,
        ]
    ]
];