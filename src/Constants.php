<?php

namespace Fstar\Task;

class Constants {
    const conf_name = 'fstar-task';
    const lib_task_exec = 'fs_task_task_exec';
    const lib_task_driver = 'fs_task_driver';
}