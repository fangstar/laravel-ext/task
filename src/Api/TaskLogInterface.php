<?php

namespace Fstar\Task\Api;

interface TaskLogInterface {
    public function __construct(array $config);

    public function logFind(string $rec_id): array;

    public function logSave(array $log);

}