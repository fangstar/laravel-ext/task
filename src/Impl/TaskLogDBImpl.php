<?php

namespace Fstar\Task\Impl;

use Fstar\Task\Api\TaskLogInterface;
use Fstar\Task\Model\TimedTaskLogM;
use Illuminate\Support\Facades\DB;

class TaskLogDBImpl implements TaskLogInterface {

    public function __construct(array $config) {
    }

    public function logFind(string $rec_id): array {
        return TimedTaskLogM::where('timed_task_record_id', $rec_id)
                            ->orderBy('log_order')
                            ->get(['log_type', 'log_content', DB::raw('from_unixtime(created_at) as created_at_text')])
                            ->toArray();
    }

    public function logSave(array $log) {
        return TimedTaskLogM::insert($log);
    }
}
