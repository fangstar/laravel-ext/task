<?php

namespace Fstar\Task\Task;


abstract class SimpleBaseTaskService extends BaseTaskService {
    protected $exec_params = null;
    protected $now = null;

    public function __construct() {
        parent::__construct('exec');
    }

    public function exec($params = null) {
        $this->now = time();
        $this->info("exec_params: {$params}");
        $this->exec_params = $this->parseExecParams($params);
        $clazz = get_called_class();
        $this->initTaskMonitor("{$clazz}::exec");
        try {
            $this->start();
            $this->finish();
        } catch (\Exception $ex) {
            $this->setError($ex->getMessage());
            $this->error($ex->getTraceAsString());
        }
    }

    protected function parseExecParams($params) {
        if (empty($params)) {
            $params = '{}';
        }
        try {
            return json_decode($params);
        } catch (\Exception $ex) {

        }
        return $params;
    }

    protected abstract function start();

}
