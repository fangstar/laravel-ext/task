<?php

namespace Fstar\Task\Task;

use Fstar\Task\Api\TaskLogInterface;
use Fstar\Task\Api\TaskRecordInterface;
use Fstar\Task\Api\TaskDefInterface;

class TaskDriverParseService {
    private $config = null;

    public function __construct(array $config) {
        $this->config = $config;
    }

    public function getTaskDefDriver(): TaskDefInterface {
        return $this->createDriver($this->config['task_def'], TaskDefInterface::class);
    }

    public function getTaskRecordDriver(): TaskRecordInterface {
        return $this->createDriver($this->config['task_record'], TaskRecordInterface::class);
    }


    public function getTaskLogDriver(): TaskLogInterface {
        return $this->createDriver($this->config['task_log'], TaskLogInterface::class);
    }

    private function createDriver($config, $clazz) {
        $driver_class = data_get($config['drivers'], $config['driver']);
        $oReflectionClass = new \ReflectionClass($driver_class);
        $svc = $oReflectionClass->newInstance($config);
        if(!($svc instanceof $clazz)) {
            throw new TaskException("Task {$driver_class} isn't instance of {$clazz}");
        }
        return $svc;
    }

}