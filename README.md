# Fstar Task

#### 介绍
房星科技定时任务组件

#### 软件架构
laravel


#### 安装教程
###### 执行SQL脚本
``` php
$vendor_path/fstar/task/src/Resource/task.sql
```
###### 发布文件配置
``` php
1. composer require fstar/task
2. php artisan vendor:publish --provider=Fstar\Task\TaskServiceProvider
3. .env 文件增加配置

   TASK_DB_CONN=数据库连接
   TASK_REDIS_CONN=REDIS库连接
   
   TASK_PROJ=
   TASK_SYS_PROJ=
   TASK_SYS_PROJ_MODULE=
```

#### 使用说明
###### 增加自动任务执行
``` php
App\Console\Kernel::schedule 方法中增加
FsTask::run($schedule);
```

###### 自定义定时任务
``` php
1.创建任务类 extends Fstar\Task\Task\SimpleBaseTaskService
2.在表timed_task_def 插入对应的记录
```

#### 参与贡献

1.  房星科技